# TimeTracker sample project for QA Automation training
So you probably is doing the QA Automation training and want to apply the knowledge acquired to test an application - you came to the right place.
This sample application imitates (and has some intentional bugs to be found) TimeTracker.
## Where to start?
1. First [fork] this repository into one under your user
2. Clone the forked repo into your machine
3. In case you don't have it installed, install:
    * [Python]
    * [Pipenv]
4. Install the dependencis of this project with the command
```sh
pipenv install
```
4. Open a Pipenv shell using the command
```sh
pipenv shell
```
5. Bring the application up with the command
```sh
cd timetracker_project
python manage.py runserver
```
6. The application is available at [https://localhost:8080/]

## How could I configure the system?
There are 3 already created users:

| User | Password |
| ------ | ------ |
| admin | admin |
| dev1 | 1234567* |
| mgr1 | 1234567* |

_dev1_ is a regular user which will track hours into the project "QA Automation", managed by the user _mgr1_

But if you want more users, projects and so on, you can log into [https://localhost:8080/admin/] using the admin user

[fork]: https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/
[Python]: https://www.python.org/downloads/
[Pipenv]: https://pypi.org/project/pipenv/
[https://localhost:8080/]:https://localhost:8080/
[https://localhost:8080/admin/]:https://localhost:8080/admin/