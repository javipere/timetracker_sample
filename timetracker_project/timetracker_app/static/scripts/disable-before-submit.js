﻿function disableAllControls() {
    let elementsList = document.querySelectorAll('[data-avoiddblclk],a,input,button,textarea,select');
    elementsList.forEach(function (element) {
        element.disabled = true;
        element.setAttribute('disabled', true);
    });
}

var priorWindowOnbeforeunload = window.onbeforeunload;
window.onbeforeunload = function () {
    disableAllControls();
    if (priorWindowOnbeforeunload)
        priorWindowOnbeforeunload.call(window);
};