from django.shortcuts import get_object_or_404, render, redirect

from django.http import HttpResponse

from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Appointment, Developer
from .forms import AppointmentForm

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def track_hours(request):
    return HttpResponse("You're about to appoint some hours.")


class AppointmentListView(LoginRequiredMixin, ListView):

    model = Appointment
    context_object_name = 'appointments'
    paginate_by = 20
       
    
    def get_queryset(self):
        queryset = Appointment.objects.filter(developer__user__id=self.request.user.id)
        return queryset

class AppointmentCreateView(LoginRequiredMixin, CreateView):

    model = Appointment
    form_class = AppointmentForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        appointment = form.save(commit=False)
        appointment.developer = Developer.objects.get(user__id=self.request.user.id)
        appointment.save()
        return redirect('index')

class AppointmentUpdateView(LoginRequiredMixin, UpdateView):

    model = Appointment
    form_class = AppointmentForm
    success_url = reverse_lazy('index')
    pk_url_kwarg = 'appointment_pk'
    context_object_name = 'appointment'

    def form_valid(self, form):
        appointment = form.save(commit=False)
        appointment.developer = Developer.objects.get(user__id=self.request.user.id)
        appointment.save()
        return redirect('index')

class AppointmentDeleteView(LoginRequiredMixin, DeleteView):

    model = Appointment
    success_url = reverse_lazy('index')
    pk_url_kwarg = 'appointment_pk'
    context_object_name = 'appointment'
    